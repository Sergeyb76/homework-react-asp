import axios from 'axios';
import React, { useEffect, useState } from 'react';

export interface IWeather {
  date: Date;
  temperatureC: number;
  temperatureF: Number;
  summary: string;
}

function App() {
  const [fetchedData, setFetchedData] = useState<IWeather[]>([]);
  useEffect(() => {
    const getData = async () => {
      const data = await axios.get<IWeather[]>(
        'https://localhost:44313/WeatherForecast'
      );
      setFetchedData(JSON.parse(data.request.response));
    };
    getData();
  }, []);

  const result = fetchedData.map((item) => {
    return (
      <tr>
        <td>{item.summary}</td>
        <td>{item.temperatureC}</td>
        <td>{item.date}</td>
      </tr>
    );
  });

  return <table>{result}</table>;
}

export default App;
